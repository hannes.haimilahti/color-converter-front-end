import './style.css'
import typescriptLogo from './typescript.svg'
import viteLogo from '/vite.svg'

document.querySelector<HTMLDivElement>('#app')!.innerHTML = `
  <div>
    <div class="converter">
      <div id="colorDisplay">
      </div>
      <div class="right">
        <div class="hex2rgb">
          <p>Hex to RGB</p>
          <div class="io">
            <input type="text" id="hexInput"></input>
            <span>&#8594;</span>
            <input type="text" id="rgbOutput"></input>
          </div>
        </div>
        <div class="rgb2hex">
          <p>RGB to Hex</p>
          <div class="io">
            <input type="text" id="redInput"></input>
            <input type="text" id="greenInput"></input>
            <input type="text" id="blueInput"></input>
            <span>&#8594;</span>
            <input type="text" id="hexOutput"></input>
          </div>
        </div>
      </div>
    <div class="madeWith">
      <span>Made with</span>
      <a href="https://vitejs.dev" target="_blank">
        <img src="${viteLogo}" class="logo" alt="Vite" />
      </a>
      <a href="https://www.typescriptlang.org/" target="_blank">
        <img src="${typescriptLogo}" class="logo vanilla" alt="TypeScript" />
      </a>
    </div>
  </div>
`

const API_BASE = "http://localhost:3000/api/v1/";

const showColor = (color : string, outputElement : HTMLInputElement) => {
  const boxElem = document.querySelector("#colorDisplay") as HTMLDivElement;
  boxElem.style.cssText = "background-color: " + color;
  outputElement.value = color;
  outputElement.style.cssText = "border-color: " + color + "; color: " + color;
};

const convertHex2Rgb = () => {
  const hex = (document.querySelector("#hexInput") as HTMLInputElement).value;
  const outputElem = document.querySelector("#rgbOutput") as HTMLInputElement;

  let validInput = /^#(\d|[a-f]){6}$/i.test(hex);
  
  if (validInput) {
    const urlHex = "%23" + hex.slice(1);
    fetch(API_BASE + `hex-to-rgb?hex=${urlHex}`).then(async (response) => {
      let rgb = await response.text();
      showColor(await rgb, outputElem);
    });
  } else {
    outputElem.value = "???";
  }
};
document.querySelector("#hexInput")?.addEventListener("change", convertHex2Rgb);

const convertRgb2Hex = () => {
  const red = (document.querySelector("#redInput") as HTMLInputElement).value;
  const green = (document.querySelector("#greenInput") as HTMLInputElement).value;
  const blue = (document.querySelector("#blueInput") as HTMLInputElement).value;
  const outputElem = document.querySelector("#hexOutput") as HTMLInputElement;

  let validInput = true;
  const check = (value : string) => {
    if (parseInt(value) < 0 || parseInt(value) > 255) {
      validInput = false;
    }
    if (isNaN(parseInt(value))) {
      validInput = false;
    }
  };
  check(red);
  check(green);
  check(blue);
  
  if (validInput) {
    fetch(API_BASE + `rgb-to-hex?red=${red}&green=${green}&blue=${blue}`).then(async (response) => {
      let hex = await response.text();
      showColor(await hex, outputElem);
    });
  } else {
    outputElem.value = "???";
  }
};
document.querySelector("#redInput")?.addEventListener("change", convertRgb2Hex);
document.querySelector("#greenInput")?.addEventListener("change", convertRgb2Hex);
document.querySelector("#blueInput")?.addEventListener("change", convertRgb2Hex);
